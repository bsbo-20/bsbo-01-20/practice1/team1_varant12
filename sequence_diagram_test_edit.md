```plantuml
@startuml sequence_diagram_test_edit



actor Пользователь as user
participant "TestSystem" as sys
participant "TestSet" as exe
participant "Test" as one

activate user
activate sys
activate exe


user -> sys: addTest(value)

sys -> exe: createTest(value)
create one
exe -> one: Test(test_id, value)
activate one
one--> exe: Test
deactivate one
'exe->exe: addTest(test_id)
exe --> sys : OK
sys --> user : OK


user -> sys: editTest(test_id, value)
sys -> exe: editTest(test_id, value)
exe -> one: edit(value)
activate one
one --> exe: OK
deactivate one
exe --> sys : OK
sys --> user : OK

user -> sys: deleteTest(test_id)
sys -> exe: deleteTest(test_id)
exe -> one: delete()
activate one
one --> exe: OK
destroy one
exe --> sys : OK
sys --> user : OK

@enduml
```