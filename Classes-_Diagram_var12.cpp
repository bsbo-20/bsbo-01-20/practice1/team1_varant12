﻿#include <iostream>
#include <string>
#include <vector>
using namespace std;

union var {
    int intVar;
    bool boolVar;
    float floatVar;
public:    
    var(int intValue)
    {
        this->intVar = intValue;
    }

    var()
    {
        this->boolVar = false;
    }
    
    ~var() {
        //cout << "var";
    };
};

class InputProgram
{
    string text;
    vector<var> parameters;
    int preferredTime;
public:
    InputProgram(string text)
    {
        cout << "Constructor of InputProgram\n";
    }

    InputProgram()
    {
        cout << "Constructor of InputProgram\n";
    }

    int Execute(var param)
    {
        int res = rand();
        cout << "InputProgram.Execute is running. Result(" << param.intVar << ") = " << res << "\n";
        return res;
    }
};

class Test 
{
    /*struct Params {
        int value;
    };*/
    int id;
    int executionTime;
    string error = "";
    bool isFinished = false;
    bool exceededTimeout = false;    
public:
    var value;
    var result;
    Test(int test_id, var value)
    {
        cout << "Constructor of Test with parameters: test_id = " << test_id << " and value = " << value.intVar << "\n";
        this->value = value;
    }

    int Execute(InputProgram* progr)
    {
        cout << "Test.Execute is running\n";
        result = progr->Execute(this->value);
        return 0;
    }

    int Edit(var value)
    {
        cout << "Test.Edit is running with value " << value.intVar << "\n";
        this->value = value;
        return 0;
    }

    void DestroySelf()
    {
        cout << "Test.DestroySelf is running\n";
        this->~Test();
    }
};

class TestSet
{
    vector<var> standartTests;
    InputProgram* prog;
    int global_id;
public:
    vector<Test> tests;
    TestSet()
    {
        cout << "Constructor of TestSet\n";
    }
    TestSet(InputProgram *prog, vector<var> params)
    {
        cout << "Constructor of TestSet\n";
        global_id = 0;
        for (int i = 0; i < params.size(); i++)
        {
            tests.insert(tests.end(), Test(global_id++, params[i]));
        }
        this->prog = prog;
    }
    
    Test CreateTest(var value) {
        cout << "TestSet.CreateTest is running with value " << value.intVar << "\n";
        Test new_test = *new Test(global_id++, value);
        tests.insert(tests.end(), new_test);
        return new_test;
    }

    int EditTest(int test_id, var value)
    {
        cout << "TestSet.EditTest is running with test_id = " << test_id << " and value = " << value.intVar << "\n";
        tests[test_id].Edit(value);
        return 0;
    }

    int DeleteTest(int test_id)
    {
        cout << "TestSet.DeleteTest is running with test_id = " << test_id << "\n";
        tests[test_id].DestroySelf();
        return 0;
    }

    vector<Test> ExeciteAll()
    {
        cout << "TestSet.ExeciteAll is running\n";
        //tests = {};
        for (int i = 0; i < tests.size(); i ++)
        {
            (tests[i]).Execute(this->prog);
        }
        return tests;
    }


};


class Report
{
    string report;
public:
    Report()
    {
        cout << "Constructor of Report\n";
    }

    Report(TestSet* testSet)
    {
        cout << "Constructor of Report\n";
        report = "\nReport of executing program\n~~~~~~~~~~~~~~~\n";
        report += "Value : Result\n";
        for (int i = 0; i < testSet->tests.size(); i++)
        {
            report += to_string(testSet->tests[i].value.intVar) + " : " + to_string(testSet->tests[i].result.intVar) + '\n';
        }
        report += "\n~~~~~~~~~~~~~~~\nEnd of report of executing program\n";
        cout << report;
    }
    /*

    void FormReport(vector<var> results)
    {
        cout << "Report.FormReport is running\n";
    }*/
};

class TestSystem
{
    int timeLimit;
    InputProgram *program;
    TestSet *testSet;
    Report *report;
public:

    
    TestSystem()
    {
        cout << "Constructor of TestSystem\n";
    }

    Report* CreateReport()
    {
        cout << "TestSystem.CreateReport is running\n";
        report = new Report(testSet);
        return report;
    }


    void Run(string prog, vector<var> params)
    {
        cout << "TestSystem.Run is running\n";
        this->program = new InputProgram(prog);
        this->testSet = new TestSet(program, params);
    }

    int AddTest(var value)
    {
        cout << "TestSystem.AddTest is running\n";
        testSet->CreateTest(value);
        return 0;
    }

    int EditTest(int test_id, var value)
    {
        cout << "TestSystem.EditTest is running\n";
        testSet->EditTest(test_id, value);
        return 0;
    }

    int DeleteTest(int test_id)
    {
        cout << "TestSystem.DeleteTest is running\n";
        testSet->DeleteTest(test_id);
        return 0;
    }

    void StartAnalisys()
    {
        cout << "TestSystem.StartAnalisys is running\n";
        testSet->ExeciteAll();
    }
};

int main()
{
    TestSystem testSystem = *new TestSystem();
    testSystem.Run("text of program", { var(1), var(2)});
    cout << endl;
    testSystem.AddTest(var(5));
    cout << endl;
    testSystem.EditTest(0, var(3));
    cout << endl;
    testSystem.DeleteTest(0);
    cout << endl;
    testSystem.StartAnalisys();
    cout << endl;
    testSystem.CreateReport();
}

