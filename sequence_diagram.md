```plantuml
@startuml sequence_diagram


actor Пользователь as user
participant "TestSystem" as sys
participant "TestSet" as exe
participant "Test" as one
participant "Report" as report
participant "InputProgram" as prog



user -> sys: TestSystem()
user -> sys: run\n(Программа для анализа, \nДиапазон входных параметров)
activate sys
sys -> prog: InputProgram(Программа для анализа)
activate prog

create exe 
sys -> exe: TestSet(Ссылка на программу для анализа, \n диапазон входных параметров)
activate exe



' hnote over exe : Составление списка тестов
loop Составление списка тестов
    user -> sys: команды редактирования тестов
    sys -> exe: команды редактирования тестов
    exe -> one: редактированиe
    activate one
    deactivate one
end
' hnote over exe : Конец составления списка тестов


user -> sys: startAnalisys()
sys -> exe: executeAll()
' hnote over exe : начало цикла тестирования
loop Цикла тестирования
    exe-> one: execute(*program)
    activate one
    one->prog: execute(parameters)
    activate prog
    prog --> one: result
    deactivate prog

    alt#LightBlue #LightGreen Тестируемая программа завершается \nбез критических ошибок
        one-> one: Полученное от программы значение, \nвремя выполнения
    else #IndianRed Тестируемая программа завершается \nс критической ошибкой
        one-> one: Текст ошибки, \nвремя выполнения
    else #LightCoral Тестируемая программа завершается \nпо таймауту
        one-> one: Текст ошибки
    end
    one --> exe: FINFISHED
    deactivate one
    deactivate prog
end
' hnote over exe : конец цикла тестирования

exe --> sys : результаты

deactivate exe

create report
user-> report: createReport()

activate report
hnote over report : вывод в консоль результатов

deactivate report
deactivate sys

@enduml
```